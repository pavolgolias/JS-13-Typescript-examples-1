"use strict";
console.log("It works!");
// string
var myName = 'Max';
var myName2 = 'Max';
//myName = 5;
// number - can be 27 or 27.1
var myAge = 27.1;
//myAge = 'Age';
// boolean
var hasHobbies = true;
//hasHobbies = 1;
//hasHobbies = 'hobbies';
// assign types
var myRealAge; // ANY TYPE
myRealAge = 27;
myRealAge = '27';
var myNumber; // NUMBER TYPE
myNumber = 27;
//myNumber = '27';
// arrays
var hobbies = ["Sports", "Cooking"];
hobbies = [100];
//hobbies = 100;
console.log(hobbies);
console.log(typeof hobbies);
// tuples - array with mixed types
var address = ["Superstreet", 99];
// enums
var Color;
(function (Color) {
    Color[Color["Gray"] = 0] = "Gray";
    Color[Color["Green"] = 1] = "Green";
    Color[Color["Blue"] = 2] = "Blue"; // 2
})(Color || (Color = {}));
var Color2;
(function (Color2) {
    Color2[Color2["Gray"] = 0] = "Gray";
    Color2[Color2["Green"] = 100] = "Green";
    Color2[Color2["Blue"] = 101] = "Blue"; // 101
})(Color2 || (Color2 = {}));
var myColor = Color.Blue;
console.log(myColor);
// ANY type
var car = "BMW";
console.log(car);
car = { brand: "BMW", series: 3 };
console.log(car);
// *** FUNCTIONS ***
function returnMyName() {
    return myName;
}
console.log(returnMyName());
// function - void
function sayHello() {
    console.log("Hello!");
    //return "age";
}
sayHello();
// function - argument types
function multiply(value1, value2) {
    return value1 * value2;
}
console.log(multiply(2, 5));
// function types
var myMultiply;
// myMultiply = sayHello;
// myMultiply();
myMultiply = multiply;
console.log(myMultiply(5, 5));
// objects
var userData = {
    name: "Max",
    age: 27
};
/*userData = {
  a: "hello",
  b: 11
};*/
// complex object
var complex = {
    data: [100, 3.99, 10],
    output: function (all) {
        return this.data;
    }
};
var complex2 = {
    data: [100, 3.99, 10],
    output: function (all) {
        return this.data;
    }
};
// union types
var realAge = 27;
realAge = "27";
//realAge = true;
// check types
var finalValue = "A string";
if (typeof finalValue == "string") {
    console.log("Final value is a string");
}
// never type
function neverReturns() {
    // console.log("asd");
    throw new Error("An error!");
}
// nullable types
var canBeNull = 12;
canBeNull = null;
var canAlsoBeNull;
canAlsoBeNull = null;
var canThisBeAny = null;
canThisBeAny = "any";
