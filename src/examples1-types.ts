console.log("It works!");

// string
let myName = 'Max';
let myName2: string = 'Max';
//myName = 5;

// number - can be 27 or 27.1
let myAge = 27.1;
//myAge = 'Age';

// boolean
let hasHobbies = true;
//hasHobbies = 1;
//hasHobbies = 'hobbies';

// assign types
let myRealAge;  // ANY TYPE
myRealAge = 27;
myRealAge = '27';

let myNumber: number;  // NUMBER TYPE
myNumber = 27;
//myNumber = '27';

// arrays
let hobbies: any[] = ["Sports", "Cooking"];
hobbies = [100];
//hobbies = 100;
console.log(hobbies);
console.log(typeof hobbies);

// tuples - array with mixed types
let address: [string, number] = ["Superstreet", 99];

// enums
enum Color {
  Gray,   // 0
  Green,  // 1
  Blue    // 2
}

enum Color2 {
  Gray,         // 0
  Green = 100,  // 100
  Blue          // 101
}

let myColor: Color = Color.Blue;
console.log(myColor);

// ANY type
let car: any = "BMW";
console.log(car);

car = { brand: "BMW", series: 3 };
console.log(car);

// *** FUNCTIONS ***
function returnMyName(): string {
  return myName;
}

console.log(returnMyName());

// function - void
function sayHello(): void {
  console.log("Hello!");
  //return "age";
}

sayHello();

// function - argument types
function multiply(value1: number, value2: number): number {
  return value1 * value2;
}

console.log(multiply(2, 5));


// function types
let myMultiply: (a: number, b: number) => number;
// myMultiply = sayHello;
// myMultiply();
myMultiply = multiply;
console.log(myMultiply(5, 5));


// objects
let userData: { name: string, age: number } = {
  name: "Max",
  age: 27
};

/*userData = {
  a: "hello",
  b: 11
};*/


// complex object
let complex: { data: number[], output: (all: boolean) => number[] } = {
  data: [100, 3.99, 10],

  output: function (all: boolean): number[] {
    return this.data;
  }
};
//complex = {};


// type alias
type Complex = { data: number[], output: (all: boolean) => number[] }

let complex2: Complex = {
  data: [100, 3.99, 10],

  output: function (all: boolean): number[] {
    return this.data;
  }
};


// union types
let realAge: number | string = 27;
realAge = "27";
//realAge = true;


// check types
let finalValue = "A string";
if ( typeof finalValue == "string" ) {
  console.log("Final value is a string");
}


// never type
function neverReturns(): never {
  // console.log("asd");
  throw new Error("An error!");
}


// nullable types
let canBeNull: number | null = 12;
canBeNull = null;

let canAlsoBeNull;
canAlsoBeNull = null;

let canThisBeAny = null;
canThisBeAny = "any";
